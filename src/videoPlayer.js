export default

class videoPlayer {
  constructor() {
    this.name = "video";
    this.player = "";
    this.time = 0.0;
    this.state = "PAUSED";
    this.playbackRate = 1.0;
  }

  createVideoPlayer(player) {
    this.player = player;
  }

  play() {
    this.player.playVideo();
  }

  pause() {
    this.player.pauseVideo();
  }

  stop() {
    this.player.stopVideo();
  }

  seekTo(seconds) {
    this.player.seekTo(seconds, true);
  }

  setPlaybackRate(playbackRate) {
    this.player.setPlaybackRate(playbackRate);
  }

  getState() {
    this.player.getPlayerState().then(state => {
      switch (state) {
        case -1:
          this.state = "UNSTARTED";
          break;
        case 0:
          this.state = "ENDED";
          break;
        case 1:
          this.state = "PLAYING";
          break;
        case 2:
          this.state = "PAUSED";
          break;
        case 3:
          this.state = "BUFFERING";
          break;
        case 5:
          this.state = "VIDEO CUED";
          break;
      }
    });
  }

}
