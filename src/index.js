import Vue from "vue";
import App from "./App.vue";
import VueYoutube from "vue-youtube";
import VModal from 'vue-js-modal'
import Log from "console-log-div";

Vue.use(VueYoutube);
Vue.use(VModal);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
