import {Howl, Howler} from 'howler';

export default

class audioPlayer {
  constructor() {
    this.name = "audio";
    this.player = [];
    this.time = 0.0;
    this.playbackRate = 1.0;
    this.volume = 1.0;
    this.fadeDuration = 2000;
    this.path = "assets/sound/";
    this.sources = [
      this.path + "dial_intro",
      this.path + "dial_solo_hugo",
      this.path + "variation_old_school1",
      this.path + "variation_old_school_v2",
      this.path + "theme_v3",
      this.path + "variation_trance",
      this.path + "trance_to_dark"
    ],
    this.currentSourceId = 0;
    this.currentSourceFile = this.sources[0];
    this.currentSourceState = "UNLOADED";
    this.stateArray = [];
  }

  // EVENT LISTENERS
  onLoad(source) {
    console.log(this.name + "| LOADED: " + source);

    this.setLoadedSourceNameAll();

    this.getLoadStateAll();
  }

  onLoadError(source) {
    console.log(this.name + "| LOAD ERROR: " + source);
  }

  onPlayError(source) {
    console.log(this.name + "| PLAY ERROR: " + source);
  }

  onPlay(source) {
    console.log(this.name + "| PLAY: " + source);
  }

  onPause(source) {
    console.log(this.name + "| PAUSE: " + source);
  }

  onStop(source) {
    console.log(this.name + "| STOP: " + source);
  }

  onFade(source) {
    let id = this.getIdBySourceName(source);

    if (this.player[id].volume() > 0.5 && this.player[id].volume() < 1.0) {
      // this.player[id].stop();
      console.log(this.name + "| FADE OUT: " + source);
    }
    if (this.player[id].volume() < 0.5) {
      console.log(this.name + "| FADE IN: " + source);
    }
  }

  onEnd(source) {
    console.log(this.name + "| END: " + source);
  }

  onMute(source) {
    console.log(this.name + "| MUTE: " + source);
  }

  onVolume(source) {
    console.log(this.name + "| VOLUME: " + source);
  }

  onRate(source) {
    console.log(this.name + "| PLAYBACK RATE: " + source);
  }

  onSeek(source) {
    console.log(this.name + "| SEEK: " + source);
  }

  onUnlock(source) {
    console.log(this.name + "| UNLOCK: " + source);
  }

  // CUSTOM EVENTS
  async createAudioPlayer() {
    let _this = this;

    let source = this.currentSourceFile;

    let promise = new Promise((resolve, reject) => {
      this.player.push(new Howl({
        src: [source + ".webm", source + ".mp3"],
        autoplay: false,
        loop: this.setSourceLoop(source),
        source: source,
        onload: function() {
          _this.onLoad(source);
          
          resolve(_this.player[_this.currentSourceId]);
        },
        onloaderror: function() {
          _this.onLoadError(source);

          reject(source);
        },
        onplayerror: function() {
          _this.onPlayError(source);
        },
        onplay: function() {
          _this.onPlay(source);
        },
        onpause: function() {
          _this.onPause(source);
        },
        onstop: function() {
          _this.onStop(source);
        },
        onend: function() {
          _this.onEnd(source);
        },
        onfade: function() {
          _this.onFade(source);
        },
        onmute: function() {
          _this.onMute(source);
        },
        onvolume: function() {
          _this.onVolume(source);
        },
        onrate: function() {
          _this.onRate(source);
        },
        onseek: function() {
          _this.onSeek(source);
        },
        onunlock: function() {
          _this.onUnlock(source);
        }
      }));
    })
    let result = await promise;

    return result;
  }

  async createAudioSource(source) {
    let _this = this;

    let promise = new Promise((resolve, reject) => {
      this.player.push(new Howl({
        src: [source + ".webm", source + ".mp3"],
        autoplay: false,
        loop: this.setSourceLoop(source),
        onload: function() {
          _this.onLoad(source);

          resolve(source);
        },
        onloaderror: function() {
          _this.onLoadError(source);

          reject(source);
        },
        onplayerror: function() {
          _this.onPlayError(source);
        },
        onplay: function() {
          _this.onPlay(source);
        },
        onpause: function() {
          _this.onPause(source);
        },
        onstop: function() {
          _this.onStop(source);
        },
        onend: function() {
          _this.onEnd(source);
        },
        onfade: function() {
          _this.onFade(source);
        },
        onmute: function() {
          _this.onMute(source);
        },
        onvolume: function() {
          _this.onVolume(source);
        },
        onrate: function() {
          _this.onRate(source);
        },
        onseek: function() {
          _this.onSeek(source);
        },
        onunlock: function() {
          _this.onUnlock(source);
        }
      }));
    })

    let result = await promise;

    return result;
  }

  wait() {
    return new Promise(r => setTimeout(r, 100));
  }

  loadAudioSources() {
    let _this = this;

    let loadChain = Promise.resolve();

    for (let source of this.sources) {
      if (source != this.currentSourceFile) {
        loadChain = loadChain.then(()=>_this.createAudioSource(source))
          .then(_this.wait)
      }
    }

    return loadChain;
  }

  play(sourceId) {
    if(!this.player[sourceId].playing()) this.player[sourceId].play();
  }

  playAll() {
    for (let i = 0; i < this.stateArray.length; i++) {
      if(this.stateArray[i].state == "PAUSED") this.player[i].play();
    }
  }

  pause(sourceId) {
    if(this.player[sourceId].playing()) this.player[sourceId].pause();
  }

  pauseAll() {
    for (let i = 0; i < this.stateArray.length; i++) {
      if(this.stateArray[i].state == "PLAYING") this.player[i].pause();
    }
  }

  stop(sourceId) {
    this.player[sourceId].stop();
  }

  stopAll() {
    for (let i = 0; i < this.player.length; i++) {
      this.player[i].stop();
    }
  }

  fadeIn(sourceId) {
    if(!this.player[sourceId].playing()) {
      this.player[sourceId].volume(0.0);
      this.player[sourceId].play();
      this.player[sourceId].fade(0.0, 1.0, this.fadeDuration);
    }
  }

  fadeInAll() {
    for (let i = 0; i < this.stateArray.length; i++) {
      if(this.stateArray[i].state == "PAUSED") {
        this.player[i].volume(0.0);
        this.player[i].play();
        this.player[i].fade(0.0, 1.0, this.fadeDuration);  
      } 
    }
  }

  fadeOut(sourceId) {
    this.player[sourceId].fade(1.0, 0.0, this.fadeDuration);
  } 

  mute(sourceId) {
    this.player[sourceId].mute();
  }

  setVolume(sourceId, val = 1.0) {
    this.player[sourceId].volume(val);
  }

  setRate(sourceId, val = 1.0) {
    this.player[sourceId].rate(val);
  }

  seek(sourceId, val) {
    this.player[sourceId].seek(val);
  }

  getLoadState(sourceId) {
    if (this.player[sourceId].state() == "unloaded") {
      this.stateArray[sourceId].state = "UNLOADED";
    }
    if (this.player[sourceId].state() == "loading") {
      this.stateArray[sourceId].state = "LOADING";
    }
    if (this.player[sourceId].state() == "loaded") {
      this.stateArray[sourceId].state = "LOADED";
    }

    this.currentSourceState = this.stateArray[this.currentSourceId].state;
  }

  getLoadStateAll() {
    for (let i = 0; i < this.stateArray.length; i++) {
      if (this.player[i].state() == "unloaded") {
        this.stateArray[i].state = "UNLOADED";
      }
      if (this.player[i].state() == "loading") {
        this.stateArray[i].state = "LOADING";
      }
      if (this.player[i].state() == "loaded") {
        this.stateArray[i].state = "LOADED";
      }
    }

    this.currentSourceState = this.stateArray[this.currentSourceId].state;
  }

  getStateBySourceName(source) {
    let state;

    for (let i = 0; i < this.stateArray.length; i++) {
      if (source == this.stateArray[i].source) state = this.stateArray[i].state;
    }

    return state;
  }

  getIdBySourceName(source) {
    let id;

    for (let i = 0; i < this.stateArray.length; i++) {
      if (source == this.stateArray[i].source) id = i;
    }

    return id;
  }

  setSourceLoop(source) {
    let loop;

    if (source == this.path + "dial_intro" || source == this.path + "dial_solo_hugo") {
      loop = false;
    } else {
      loop = true;
    }

    return loop;
  }

  setLoadedSourceNameAll() {
    let src;

    for (let i = 0; i < this.player.length; i++) {
      src = this.player[i]._src;
      src.slice(-1) == "3" ? this.stateArray[i] = {source: src.slice(0, -4)} : this.stateArray[i] = {source: src.slice(0, -5)};
    }
  }

  setPlaybackState(sourceId) {
    if (this.player[sourceId].playing()) {
      this.stateArray[sourceId].state = "PLAYING";
    }
    if (!this.player[sourceId].playing() && this.player[sourceId].seek() > 0) {
      this.stateArray[sourceId].state = "PAUSED";
    }
    if (this.stateArray[sourceId].state != "PAUSED" && this.player[sourceId].seek() == 0) {
      this.stateArray[sourceId].state = "STOPPED";
    }

    this.currentSourceState = this.stateArray[this.currentSourceId].state;
  }

  setPlaybackStateAll() {
    for (let i = 0; i < this.stateArray.length; i++) {
      if (this.player[i].playing()) {
        this.stateArray[i].state = "PLAYING";
      }
      if (!this.player[i].playing() && this.player[i].seek() > 0) {
        this.stateArray[i].state = "PAUSED";
      }
      if (this.stateArray[i].state != "PAUSED" && this.player[i].seek() == 0) {
        this.stateArray[i].state = "STOPPED";
      }  
    }

    this.currentSourceState = this.stateArray[this.currentSourceId].state;
  }
}
