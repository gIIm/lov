import axios from "axios";

export default

class timeline {
  constructor() {
    this.name = "timeline";
    this.data = {};
    this.currentTime = Date.now();
    this.currentDialogTime = Date.now();
    this.currentDialogSoloTime = Date.now();
    this.previousTime = 0;
    this.previousDialogTime = 0;
    this.previousDialogSoloTime = 0;
    this.elapsedTime = 0.0;
    this.elapsedDialogTime = 0.0;
    this.elapsedDialogSoloTime = 0.0;
    this.totalTime = 223;
    this.progress = 0;
    this.videoPlayerDuration = 1796;
    this.delayThreshold = 0.2;
    this.mode = "DIALOG",
    this.scenes = [
      "conversation début",
      "dialogue client/livreur",
      "dejeuner sur l'herbe",
      "salle des fantasmes",
      "parc bucolique",
      "déballage du colis",
      "retour à la conversation",
      "fin"
    ];
    this.scene = null;
    this.timecode = null;
    this.timelineMin = null;
    this.timelineMax = null;
    this.timelineDialogMax = null;
    this.timelineAutoMax = null;
    this.timelineRandomMin = 150;
    this.timelineRandomMax = 168;
    this.currentDialogId = 0;
    this.currentAutoId = 0;
    this.currentPartId = 0;
    this.currentMarkerId = 0;
    this.currentRandomId = 0;
    this.isDialogMode = true,
    this.isDialogEnsemble = true,
    this.isDialogEnsembleEnd = false;
    this.isDialogSoloHugo = false,
    this.isDialogSoloCharles = false,
    this.isAutoMode = true,
    this.isInteractiveMode = false,
    this.isTransitionSelected = false;
    this.isMarkerSelected = false;
    this.isFirstHugoTransition = true;
    this.isFirstCharlesTransition = true;
    this.isHoverboardTransition = false;
    this.isHoverboardChuteTransition = false;
    this.isCartonZoomTransition = false;
    this.isTerminated = false;
    this.markerCounter = 0;
    this.markerCounterReset = false;
    this.markerLimitTimecode = 1795;
    this.markerSelectLimit = null;
    this.markerSelectDelay = 2000;
  }

  async getTimelineData() {
    let _this = this;

    let promise = new Promise((resolve, reject) => {      
      axios
        .get("assets/JSON/timeline.json")
        .then(response => {
          _this.data = response.data;
          _this.timecode = _this.data.dialog[this.currentDialogId].timecode;
          _this.scene = _this.data.dialog[this.currentDialogId].scene;
          // console.log(_this.name + " | LOADED: ", JSON.parse(JSON.stringify(_this.data)));
          console.log(_this.name + " | LOADED");
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        })
    });

    let result = await promise;

    return result;
  }

  getTimelineMode() {
    if (this.isDialogMode) {
      this.mode = "DIALOG";
    } else if (!this.isDialogMode && this.isAutoMode) {
      this.mode = "AUTO";
    } else if (this.isInteractiveMode) {
      this.mode = "INTERACTIVE";
    }
  }

  updateTime(videoPlayer, audioPlayer) {
    if ( videoPlayer.state == "PLAYING") { 
      // elapsed dialog time
      if (this.isDialogMode) {
        if (this.isDialogEnsemble || this.isDialogEnsembleEnd) {
          this.elapsedDialogTime = parseFloat(((Date.now() - this.currentDialogTime)/1000) + this.previousDialogTime).toFixed(2);
        }
      
        if (this.isDialogSoloHugo || this.isDialogSoloCharles) {
          if(videoPlayer.time >= 147) this.elapsedDialogSoloTime = parseFloat(((Date.now() - this.currentDialogSoloTime)/1000) + this.previousDialogSoloTime).toFixed(2);
        }  
      }  
      
      // elapsed time 
      if (!this.isDialogMode) {
        this.elapsedTime = parseFloat(((Date.now() - this.currentTime)/1000) + this.previousTime).toFixed(2);
      }
    } else { 
      // save currentTime 
      if (this.isDialogMode) {
        if (this.isDialogEnsemble || this.isDialogEnsembleEnd) {
          this.currentDialogTime = Date.now();
          this.previousDialogTime = parseFloat(this.elapsedDialogTime);  
        }
        if (this.isDialogSoloHugo || this.isDialogSoloCharles) {
          this.currentDialogSoloTime = Date.now();
          this.previousDialogSoloTime = parseFloat(this.elapsedDialogSoloTime);
        }  
      }
      if (!this.isDialogMode) {
        this.currentTime = Date.now(); 
        this.previousTime = parseFloat(this.elapsedTime);
      }
    }

    let progress = (((this.elapsedTime) / this.totalTime) * 100).toFixed(2);
    this.progress = progress < 100 ? progress : 100;

    audioPlayer.time = audioPlayer.player[audioPlayer.currentSourceId].seek().toFixed(2);

    this.fixStreamDelay(videoPlayer, audioPlayer);

    this.updateTimeline(videoPlayer, audioPlayer);

    this.updateTimecode(videoPlayer, audioPlayer);
  }

  fixStreamDelay(videoPlayer, audioPlayer) {
    let delay;

    // FIX DELAY FOR SYNC CRITICAL AUDIO SOURCE ONLY (I.E. DIALOG)
    if (this.isDialogMode) {
      if (videoPlayer.time > audioPlayer.time) {
        delay = videoPlayer.time - audioPlayer.time;

        if (delay >= this.delayThreshold) {
          console.log(audioPlayer.name + " | DELAY:", delay);
          audioPlayer.player[audioPlayer.currentSourceId].seek(videoPlayer.time);
        }
      } else if (videoPlayer.time < audioPlayer.time) {
        delay = audioPlayer.time - videoPlayer.time;

        if (delay >= this.delayThreshold) {
          console.log(videoPlayer.name + " | DELAY: ", delay);
          videoPlayer.player.seekTo(audioPlayer.player[audioPlayer.currentSourceId].seek(), true);
        }
      }
    }
  }

  updateTimeline(videoPlayer, audioPlayer) {
    let _this = this;

    this.getTimelineMode();

    // DIALOG TIMELINE
    if (this.isDialogMode) {
      this.updateDialogTimeline(videoPlayer, audioPlayer);

    } else {
      // INTERACTIVE TIMELINE

      // this.setMarkerLimit(videoPlayer, audioPlayer);

      // AUTO TIMELINE
      // this.timelineAutoMax = this.data.auto[this.currentAutoId].timelineAutoMax;

      // if (this.elapsedTime > this.timelineAutoMax && this.currentAutoId < this.data.auto.length - 1) {
      //   this.currentAutoId++;

      //   console.log(this.name + " | AUTO: ", this.currentAutoId);
      // }

      // HOVERBOARD
      if (this.isHoverboardTransition) {
        if (this.elapsedTime > this.data.part[this.currentPartId].transition.timelineMax || this.markerCounterReset) {
          this.scene =  "dialogue ensemble fin";
          this.timecode = 100;

          this.resetPreviousTimecodeParameters(videoPlayer, audioPlayer);
      
          audioPlayer.currentSourceFile = audioPlayer.path + "dial_intro";

          this.checkTransitionAudioSource(audioPlayer).then(
            result => {
              _this.setPlayerNewParametersAsync(videoPlayer, audioPlayer).then(
                result => {
                  _this.isHoverboardTransition = false;

                  _this.isInteractiveMode = false;

                  _this.isDialogMode = true;
                  _this.isDialogEnsemble = true;
                  _this.isDialogEnsembleEnd = true;

                  _this.markerCounterReset = false;

                  // _this.currentPartId++;
                  _this.currentPartId = 1;
                },
                error => alert(error)
              );    
            },
            error => alert(error)
          );
        }

      }
      
      // EMBALLAGE
      if (this.isHoverboardChuteTransition) {
        if ((this.elapsedTime > this.data.part[this.currentPartId].transition.timelineMax) || this.markerCounterReset) {          
          this.isHoverboardChuteTransition = false;

          this.isInteractiveMode = false;

          this.markerCounterReset = false;

          this.currentPartId++;

          this.stopDialogTimeline(videoPlayer, audioPlayer);
        }
      }

      // CARTON ZOOM
      if (this.elapsedTime > 120 && this.isAutoMode && !this.isCartonZoomTransition) {
        this.resetPreviousTimecodeParameters(videoPlayer, audioPlayer);
        
        this.scene = "carton zoom";
        this.timecode = 740;
        
        audioPlayer.currentSourceFile = audioPlayer.path + "trance_to_dark";

        this.checkAutoAudioSource(audioPlayer).then(
          result => {
            _this.setPlayerNewParameters(videoPlayer, audioPlayer);

            _this.isCartonZoomTransition = true;
          },
          error => alert(error)
        );
      
        console.log(this.name + " | CREDITS");
      }

      // CREDITS
      if ((this.elapsedTime > this.timelineRandomMax && !this.isTerminated) || (videoPlayer.time >= 1720 && !this.isTerminated)) {        
        this.resetPreviousTimecodeParameters(videoPlayer, audioPlayer);
        
        this.scene = "crédits";
        this.timecode = 1728;
              
        audioPlayer.currentSourceFile = audioPlayer.path + "dial_solo_hugo";
        this.checkAutoAudioSource(audioPlayer).then(
          result => {
            _this.setPlayerNewParameters(videoPlayer, audioPlayer);

            _this.isTerminated = true;
          },
          error => alert(error)
        );

        console.log(this.name + " | CREDITS");
      }

      // END
      if ((this.elapsedTime > this.totalTime) || videoPlayer.time > 1780) {
        console.log(this.name + " | END");

        videoPlayer.stop();

        audioPlayer.stopAll();

        location.reload();
      }

      this.timelineMin = this.data.part[this.currentPartId].transition.timelineMin;
      this.timelineMax = this.data.part[this.currentPartId].transition.timelineMax;

      if (this.elapsedTime > this.timelineMax && this.currentPartId < this.data.part.length - 1) {
        this.currentPartId++;

        this.isTransitionSelected = false;
        console.log(this.name + " | PART: ", this.currentPartId + 1);
      }
    }
  }

  updateDialogTimeline(videoPlayer, audioPlayer) {
    let _this = this;

    if (this.isDialogEnsemble && videoPlayer.time >= 144 && videoPlayer.time <= 148) {
      this.stopDialogTimeline(videoPlayer, audioPlayer);
    }

    // if (this.isDialogSoloHugo && videoPlayer.time >= 320) {
    if (this.isDialogSoloHugo && this.elapsedDialogSoloTime >= 173) {
      this.stopDialogTimeline(videoPlayer, audioPlayer);
    }

    if (this.isDialogSoloCharles && videoPlayer.time >= 538) {
      this.stopDialogTimeline(videoPlayer, audioPlayer);
    }
  }

  async stopDialogTimeline(videoPlayer, audioPlayer) {
    let _this = this;

    let promise = new Promise((resolve, reject) => {        
      _this.isDialogMode = false;
      _this.isDialogEnsemble = false;
      // _this.isDialogEnsembleEnd = false;
      _this.isDialogSoloHugo = false;
      _this.isDialogSoloCharles = false;
  
      _this.isAutoMode = true;
  
      _this.scene = "emballage_hugo";
      _this.timecode = 540;
  
      _this.currentTime = Date.now();
  
      _this.resetPreviousTimecodeParameters(videoPlayer, audioPlayer);
      
      audioPlayer.currentSourceFile = audioPlayer.path + "variation_old_school_v2";

      _this.checkAutoAudioSource(audioPlayer).then(
        result => {
          _this.setPlayerNewParameters(videoPlayer, audioPlayer);
          
          if (!_this.isDialogEnsembleEnd) {
            _this.previousTime = 0;
          } else {
            _this.elapsedTime = parseFloat(_this.data.part[_this.currentPartId].transition.timelineMin);
            _this.previousTime = parseFloat(_this.data.part[_this.currentPartId].transition.timelineMin);
          }    
          
          resolve();
        },
        error => alert(error)
      );  
    })

    let result = await promise;

    return result;    
  }

  updateTimecode(videoPlayer, audioPlayer) {
    let _this = this;

    document.body.onkeyup = function(e) {
      if(e.keyCode == 32){        
        if (videoPlayer.state == "PLAYING") {
          if (_this.isDialogMode) {
            // DIALOG TIMELINE
            _this.updateDialogTimecode(videoPlayer, audioPlayer);
          }

          // AUTO TIMELINE
          if (!_this.isDialogMode && _this.isAutoMode) {
            _this.isAutoMode = false;
            _this.isInteractiveMode = true;
          }

          if (_this.isInteractiveMode) {
            // INTERACTIVE TIMELINE (PART 1/2/3/4/5/6/8)
            _this.setTimecodeParameters(videoPlayer, audioPlayer);
            _this.setRandomTimecodeParameters(videoPlayer, audioPlayer);  
          }          
        }
      }
    }

    let tapButtonEl = document.getElementsByClassName("tap-button");
    tapButtonEl[0].ontouchstart = function(e) {
      if (videoPlayer.state == "PLAYING") {
        if (_this.isDialogMode) {
          // DIALOG TIMELINE
          _this.updateDialogTimecode(videoPlayer, audioPlayer);
        }

        // AUTO TIMELINE
        if (!_this.isDialogMode && _this.isAutoMode) {
          _this.isAutoMode = false;
          _this.isInteractiveMode = true;
        }

        if (_this.isInteractiveMode) {
          // INTERACTIVE TIMELINE (PART 1/2/3/4/5/6/8)
          _this.setTimecodeParameters(videoPlayer, audioPlayer);
          _this.setRandomTimecodeParameters(videoPlayer, audioPlayer);  
        }
      }
    }
    tapButtonEl[1].ontouchstart = function(e) {
      if (videoPlayer.state == "PLAYING") {
        if (_this.isDialogMode) {
          // DIALOG TIMELINE
          _this.updateDialogTimecode(videoPlayer, audioPlayer);
        }

        // AUTO TIMELINE
        if (!_this.isDialogMode && _this.isAutoMode) {
          _this.isAutoMode = false;
          _this.isInteractiveMode = true;
        }

        if (_this.isInteractiveMode) {
          // INTERACTIVE TIMELINE (PART 1/2/3/4/5/6/8)
          _this.setTimecodeParameters(videoPlayer, audioPlayer);
          _this.setRandomTimecodeParameters(videoPlayer, audioPlayer);  
        }
      }
    }

    // AUTO TIMELINE (PART 7)
    // this.setAutoTimecodeParameters(videoPlayer, audioPlayer);
  }

  updateDialogTimecode(videoPlayer, audioPlayer) {
    let _this = this;

    if (this.isDialogMode && this.isDialogEnsemble && !this.isDialogEnsembleEnd) {
      this.resetPreviousTimecodeParameters(videoPlayer, audioPlayer);

      // TIMECODE
      if (this.data.dialog[this.currentDialogId].hasOwnProperty("timecode")) {
        this.timecode = this.data.dialog[this.currentDialogId].timecode;
        console.log(this.name + " | TIMECODE: ", this.timecode);
      }
      // SCENE
      if (this.data.dialog[this.currentDialogId].hasOwnProperty("scene")) {
        this.scene = this.data.dialog[this.currentDialogId].scene;
      }
      // VIDEO PLAYBACK RATE
      if (this.data.dialog[this.currentDialogId].hasOwnProperty("videoPlaybackRate")) {
        videoPlayer.playbackRate = this.data.dialog[this.currentDialogId].videoPlaybackRate;
      }
      // AUDIO PLAYBACK RATE
      if (this.data.dialog[this.currentDialogId].hasOwnProperty("audioPlaybackRate")) {
        audioPlayer.playbackRate = this.data.dialog[this.currentDialogId].audioPlaybackRate;
      }
      // AUDIO VOLUME
      if (this.data.dialog[this.currentDialogId].hasOwnProperty("volume")) {
        audioPlayer.volume = this.data.dialog[this.currentDialogId].volume;
      }
      // AUDIO TRACK
      if (this.data.dialog[this.currentDialogId].hasOwnProperty("audioTrack")) {
        audioPlayer.currentSourceFile = audioPlayer.path + this.data.dialog[this.currentDialogId].audioTrack;
      }

      this.checkDialogAudioSource(audioPlayer).then(
        result => {
          _this.setPlayerNewParameters(videoPlayer, audioPlayer);

          _this.currentDialogSoloTime = Date.now();
          _this.previousDialogSoloTime = 0;      

          _this.scene = "dialogue solo hugo";

          audioPlayer.player[audioPlayer.currentSourceId].seek(_this.timecode);

          _this.isDialogSoloHugo = true;
          _this.isDialogEnsemble = false;   
           
        },
        error => alert(error)
      );
    } else if (this.isDialogMode && this.isDialogEnsembleEnd) {
      // DIALOG ENSEMBLE END
      let _this = this;
  
      this.scene =  this.data.part[this.currentPartId].transition.scene;
      this.timecode = this.data.part[this.currentPartId].transition.timecode;
    
      this.resetPreviousTimecodeParameters(videoPlayer, audioPlayer);
  
      audioPlayer.currentSourceFile = audioPlayer.path + this.data.part[this.currentPartId].transition.audioTrack;

      this.checkTransitionAudioSource(audioPlayer).then(
        result => {
          _this.setPlayerNewParametersAsync(videoPlayer, audioPlayer).then(
            result => {
              _this.isDialogMode = false;
              _this.isDialogSoloHugo = false;

              _this.isInteractiveMode = true;

              _this.isHoverboardChuteTransition = true;

              _this.isTransitionSelected = true;

              _this.markerSelectLimit = _this.data.part[_this.currentPartId].marker.length;

              _this.currentTime = Date.now();
              _this.elapsedTime = parseFloat(_this.data.part[_this.currentPartId].transition.timelineMin);
              _this.previousTime = parseFloat(_this.data.part[_this.currentPartId].transition.timelineMin);
            },
            error => alert(error)
          );    
        },
        error => alert(error)
      );
    } else if (this.isDialogMode && !this.isDialogEnsemble && !this.isDialogSoloCharles) {
      // DIALOG SOLO HUGO
      // this.switchDialogSoloMode(videoPlayer, audioPlayer);

      let _this = this;
  
      this.scene =  this.data.part[this.currentPartId].transition.scene;
      this.timecode = this.data.part[this.currentPartId].transition.timecode;
  
      this.currentTime = Date.now();
      this.previousTime = 0;
  
      this.resetPreviousTimecodeParameters(videoPlayer, audioPlayer);
  
      audioPlayer.currentSourceFile = audioPlayer.path + this.data.part[this.currentPartId].transition.audioTrack;

      this.checkTransitionAudioSource(audioPlayer).then(
        result => {
          _this.setPlayerNewParametersAsync(videoPlayer, audioPlayer).then(
            result => {
              _this.isDialogMode = false;
              _this.isDialogSoloHugo = false;

              _this.isInteractiveMode = true;

              _this.isHoverboardTransition = true;

              _this.isTransitionSelected = true;

              _this.markerSelectLimit = _this.data.part[_this.currentPartId].marker.length;
            },
            error => alert(error)
          );    
        },
        error => alert(error)
      );
      
    } else if (this.isDialogMode && this.isDialogSoloCharles) {
      // DIALOG SOLO CHARLES
      this.switchDialogSoloMode(videoPlayer, audioPlayer);
    }
  }

  switchDialogSoloMode(videoPlayer, audioPlayer) {
    let _this = this;

    if (this.isDialogSoloHugo) {
      this.currentDialogId = 2;
      if (this.isFirstHugoTransition) {
        this.previousDialogSoloTime = parseFloat(this.elapsedDialogSoloTime);
        this.isFirstHugoTransition = false;
      } 
      let cue = parseInt(362.0 + this.previousDialogSoloTime);
      this.timecode = cue;
      // videoPlayer.time = cue;

      this.scene = "dialogue solo charles";

      this.setPlayerNewDialogParametersAsync(videoPlayer, audioPlayer).then(
        result => {
          // videoPlayer.time = result;

          _this.isDialogSoloHugo = false;
          _this.isDialogSoloCharles = true;        
        },
        error => alert(error)
      );
    } else if (this.isDialogSoloCharles) {
      this.currentDialogId = 1;
      if (this.isFirstCharlesTransition) {
        this.previousDialogSoloTime = parseFloat(this.elapsedDialogSoloTime);
        this.isFirstCharlesTransition = false;
      } 
      let cue = parseInt(147.0 + this.previousDialogSoloTime);
      this.timecode = cue;
      // videoPlayer.time = cue;
      
      this.scene = "dialogue solo hugo";

      this.setPlayerNewDialogParametersAsync(videoPlayer, audioPlayer).then(
        result => {
          // videoPlayer.time = result;
          
          _this.isDialogSoloHugo = true;
          _this.isDialogSoloCharles = false;
        },
        error => alert(error)
      );
    }
  }

  setTimecodeParameters(videoPlayer, audioPlayer) {
    let _this = this;

    if (this.isTransitionSelected && !this.isMarkerSelected) {
      if ((this.elapsedTime > this.timelineMin) && (this.elapsedTime < this.timelineMax)) {
        this.setNextMarkerParameters(videoPlayer, audioPlayer).then(
          result => {
            _this.checkMarkerAudioSource(audioPlayer).then(
              result => {
                _this.setPlayerNewParameters(videoPlayer, audioPlayer);

                // _this.markerCounter++;
              },
              error => alert(error)
            );
          },
          // error => alert(error)
          error => console.log(error)
        );
      }
    }

    if (!this.isTransitionSelected) {
      if ((this.elapsedTime > this.timelineMin) && (this.elapsedTime < this.timelineMax)) {
        this.setNextTransitionParameters(videoPlayer, audioPlayer).then(
          result => {
            _this.checkTransitionAudioSource(audioPlayer).then(
              result => {
                _this.setPlayerNewParameters(videoPlayer, audioPlayer);

                _this.isTransitionSelected = true;

                _this.markerSelectLimit = _this.data.part[this.currentPartId].marker.length;
              },
              error => alert(error)
            );
          },
          error => alert(error)
        );
      }
    }
  }

  setAutoTimecodeParameters(videoPlayer, audioPlayer) {
    let _this = this;

    if (this.elapsedTime == this.timelineAutoMax) {
      this.resetPreviousTimecodeParameters(videoPlayer, audioPlayer);

      // TIMECODE
      this.timecode = this.data.auto[this.currentAutoId].timecode;
      console.log(this.name + " | TIMECODE: ", this.timecode);
      // SCENE
      if (this.data.auto[this.currentAutoId].hasOwnProperty("scene")) {
        this.scene = this.data.auto[this.currentAutoId].scene;
      }
      // VIDEO PLAYBACK RATE
      if (this.data.auto[this.currentAutoId].hasOwnProperty("videoPlaybackRate")) {
        videoPlayer.playbackRate = this.data.auto[this.currentAutoId].videoPlaybackRate;
      }
      // AUDIO PLAYBACK RATE
      if (this.data.auto[this.currentAutoId].hasOwnProperty("audioPlaybackRate")) {
        audioPlayer.playbackRate = this.data.auto[this.currentAutoId].audioPlaybackRate;
      }
      // AUDIO VOLUME
      if (this.data.auto[this.currentAutoId].hasOwnProperty("volume")) {
        audioPlayer.volume = this.data.auto[this.currentAutoId].volume;
      }
      // AUDIO TRACK
      if (this.data.auto[this.currentAutoId].hasOwnProperty("audioTrack")) {
        audioPlayer.currentSourceFile = audioPlayer.path + this.data.auto[this.currentAutoId].audioTrack;
      }

      this.checkAutoAudioSource(audioPlayer).then(
        result => {
          _this.setPlayerNewParameters(videoPlayer, audioPlayer);
        },
        error => alert(error)
      );
    }
  }
  
  setRandomTimecodeParameters(videoPlayer, audioPlayer) {
    let _this = this;

    if (this.elapsedTime > this.timelineRandomMin && this.elapsedTime < this.timelineRandomMax && !this.isMarkerSelected) {
      this.resetPreviousTimecodeParameters(videoPlayer, audioPlayer);

      this.setRandomId();

      // TIMECODE
      this.timecode = this.data.random[this.currentRandomId].timecode;
      if (this.timecode == "random") {
        let minTimecode = 540;
        let maxTimecode = this.videoPlayerDuration - 71;
    
        this.timecode = Math.floor(Math.random() * (maxTimecode - minTimecode)) + minTimecode;
      }
      
      console.log(this.name + " | TIMECODE: ", this.timecode);
      
      // SCENE
      if (this.data.random[this.currentRandomId].hasOwnProperty("scene")) {
        this.scene = this.data.random[this.currentRandomId].scene;
      }
      // VIDEO PLAYBACK RATE
      if (this.data.random[this.currentRandomId].hasOwnProperty("videoPlaybackRate")) {
        videoPlayer.playbackRate = this.data.random[this.currentRandomId].videoPlaybackRate;
      }
      // AUDIO PLAYBACK RATE
      if (this.data.random[this.currentRandomId].hasOwnProperty("audioPlaybackRate")) {
        audioPlayer.playbackRate = this.data.random[this.currentRandomId].audioPlaybackRate;
      }
      // AUDIO VOLUME
      if (this.data.random[this.currentRandomId].hasOwnProperty("volume")) {
        audioPlayer.volume = this.data.random[this.currentRandomId].volume;
      }
      // AUDIO TRACK
      if (this.data.random[this.currentRandomId].hasOwnProperty("audioTrack")) {
        audioPlayer.currentSourceFile = audioPlayer.path + this.data.random[this.currentRandomId].audioTrack;
      }

      this.checkRandomAudioSource(audioPlayer).then(
        result => {
          _this.setPlayerNewParameters(videoPlayer, audioPlayer);

          // _this.markerCounter++;
        },
        error => alert(error)
      );
    }
  }

  async setNextTransitionParameters(videoPlayer, audioPlayer) {
    let promise = new Promise((resolve, reject) => {

      this.resetPreviousTimecodeParameters(videoPlayer, audioPlayer);

      // TIMECODE
      this.timecode = this.data.part[this.currentPartId].transition.timecode;
      console.log(this.name + " | TIMECODE: ", this.timecode);
      // SCENE
      if (this.data.part[this.currentPartId].transition.hasOwnProperty("scene")) {
        this.scene = this.data.part[this.currentPartId].transition.scene;
      }
      // VIDEO PLAYBACK RATE
      if (this.data.part[this.currentPartId].transition.hasOwnProperty("videoPlaybackRate")) {
        videoPlayer.playbackRate = this.data.part[this.currentPartId].transition.videoPlaybackRate;
      }
      // AUDIO PLAYBACK RATE
      if (this.data.part[this.currentPartId].transition.hasOwnProperty("audioPlaybackRate")) {
        audioPlayer.playbackRate = this.data.part[this.currentPartId].transition.audioPlaybackRate;
      }
      // AUDIO VOLUME
      if (this.data.part[this.currentPartId].transition.hasOwnProperty("volume")) {
        audioPlayer.volume = this.data.part[this.currentPartId].transition.volume;
      }
      // AUDIO TRACK
      if (this.data.part[this.currentPartId].transition.hasOwnProperty("audioTrack")) {
        audioPlayer.currentSourceFile = audioPlayer.path + this.data.part[this.currentPartId].transition.audioTrack;
      }    

      resolve();
    })
    
    let result = await promise;

    return result;
  }

  async setNextMarkerParameters(videoPlayer, audioPlayer) {
    let promise = new Promise((resolve, reject) => {

      this.resetPreviousTimecodeParameters(videoPlayer, audioPlayer);

      this.setCurrentMarkerId();

      // TIMECODE
      this.timecode = this.data.part[this.currentPartId].marker[this.currentMarkerId].timecode;
      console.log(this.name + " | TIMECODE: ", this.timecode);
      // SCENE
      if (this.data.part[this.currentPartId].marker[this.currentMarkerId].hasOwnProperty("scene")) {
        this.scene = this.data.part[this.currentPartId].marker[this.currentMarkerId].scene;
      }
      // VIDEO PLAYBACK RATE
      if (this.data.part[this.currentPartId].marker[this.currentMarkerId].hasOwnProperty("videoPlaybackRate")) {
        videoPlayer.playbackRate = this.data.part[this.currentPartId].marker[this.currentMarkerId].videoPlaybackRate;
      }
      // AUDIO PLAYBACK RATE
      if (this.data.part[this.currentPartId].marker[this.currentMarkerId].hasOwnProperty("audioPlaybackRate")) {
        audioPlayer.playbackRate = this.data.part[this.currentPartId].marker[this.currentMarkerId].audioPlaybackRate;
      }
      // AUDIO VOLUME
      if (this.data.part[this.currentPartId].marker[this.currentMarkerId].hasOwnProperty("volume")) {
        audioPlayer.volume = this.data.part[this.currentPartId].marker[this.currentMarkerId].volume;
      }
      // AUDIO TRACK
      if (this.data.part[this.currentPartId].marker[this.currentMarkerId].hasOwnProperty("audioTrack")) {
        audioPlayer.currentSourceFile = audioPlayer.path + this.data.part[this.currentPartId].marker[this.currentMarkerId].audioTrack;
      }    
      
      resolve();
    })

    let result = await promise;

    return result;
  }

  resetPreviousTimecodeParameters(videoPlayer, audioPlayer) {
    videoPlayer.playbackRate = 1.0;

    audioPlayer.playbackRate = 1.0;
    audioPlayer.volume = 1.0;
  }

  setCurrentMarkerId() {    
    if (this.markerCounter < this.markerSelectLimit - 1) {
      this.currentMarkerId = this.markerCounter;

      this.markerCounter++;

      this.markerCounterReset = false;
    } else {
      this.currentMarkerId = this.markerCounter;

      this.markerCounter = 0;

      this.markerCounterReset = true;
    }

    console.log(this.name + " | MARKER: ", this.currentMarkerId);
  }
  
  setRandomMarkerId() {
    let minId = 0;
    let maxId = this.data.part[this.currentPartId].marker.length;

    this.currentMarkerId = Math.floor(Math.random() * (maxId - minId)) + minId;
    
    console.log(this.name + " | MARKER: ", this.currentMarkerId);
  }

  setRandomId() {
    let minId = 0;
    let maxId = this.data.random.length;

    this.currentRandomId = Math.floor(Math.random() * (maxId - minId)) + minId;
    
    console.log(this.name + " | RANDOM: ", this.currentRandomId);
  }

  setMarkerLimit(videoPlayer, audioPlayer) {    
    if (this.markerCounter == this.markerSelectLimit) {
      console.log(this.name + " | MARKER LIMIT");

      videoPlayer.seekTo(1790);

      this.markerCounter = 0;
    }

    if (videoPlayer.time >= this.markerLimitTimecode && this.elapsedTime < this.timelineRandomMax) {
      videoPlayer.seekTo(this.timecode);
    }
  }

  async checkTransitionAudioSource(audioPlayer) {
    let promise = new Promise((resolve, reject) => {

      if (this.data.part[this.currentPartId].transition.hasOwnProperty("audioTrack")) {
        let nextAudioSourceState = audioPlayer.getStateBySourceName(audioPlayer.currentSourceFile);

        if (nextAudioSourceState != "UNLOADED" || nextAudioSourceState != "LOADING" || nextAudioSourceState != undefined) {
            // GET NEXT AUDIO TRACK ID
            let nextAudioSourceId = audioPlayer.getIdBySourceName(audioPlayer.currentSourceFile);
            if (nextAudioSourceId != undefined) {
              // THIS CURRENT AUDIO FADE OUT
              audioPlayer.stop(audioPlayer.currentSourceId);
              // audioPlayer.fadeOut(audioPlayer.currentSourceId);
              // REPLACE CURRENT AUDIO WITH NEW SOURCE AND FADE IN
              audioPlayer.currentSourceId = nextAudioSourceId;
              // audioPlayer.setVolume(audioPlayer.currentSourceId, 0.0);
              // audioPlayer.play(audioPlayer.currentSourceId);
              audioPlayer.fadeIn(audioPlayer.currentSourceId);
              
              resolve();
            }
        }
      } else {
        resolve();
      }
    })

    let result = await promise;

    return result;
  }

  async checkMarkerAudioSource(audioPlayer) {
    let promise = new Promise((resolve, reject) => {

      if (this.data.part[this.currentPartId].marker[this.currentMarkerId].hasOwnProperty("audioTrack")) {
        let nextAudioSourceState = audioPlayer.getStateBySourceName(audioPlayer.currentSourceFile);

        if (nextAudioSourceState != "UNLOADED" || nextAudioSourceState != "LOADING" || nextAudioSourceState != undefined) {
            // GET NEXT AUDIO TRACK ID
            let nextAudioSourceId = audioPlayer.getIdBySourceName(audioPlayer.currentSourceFile);
            if (nextAudioSourceId != undefined) {
              // THIS CURRENT AUDIO FADE OUT
              audioPlayer.stop(audioPlayer.currentSourceId);
              // audioPlayer.fadeOut(audioPlayer.currentSourceId);
              // REPLACE CURRENT AUDIO WITH NEW SOURCE AND FADE IN
              audioPlayer.currentSourceId = nextAudioSourceId;
              // audioPlayer.setVolume(audioPlayer.currentSourceId, 0.0);
              // audioPlayer.play(audioPlayer.currentSourceId);
              audioPlayer.fadeIn(audioPlayer.currentSourceId);
              
              resolve();
            }
        }
      } else {
        resolve();
      }
    })

    let result = await promise;

    return result;
  }

  async checkDialogAudioSource(audioPlayer) {
    let promise = new Promise((resolve, reject) => {

      if (this.data.dialog[this.currentDialogId].hasOwnProperty("audioTrack")) {
        let nextAudioSourceState = audioPlayer.getStateBySourceName(audioPlayer.currentSourceFile);

        if (nextAudioSourceState != "UNLOADED" || nextAudioSourceState != "LOADING" || nextAudioSourceState != undefined) {
            // GET NEXT AUDIO TRACK ID
            let nextAudioSourceId = audioPlayer.getIdBySourceName(audioPlayer.currentSourceFile);
            if (nextAudioSourceId != undefined) {
              // THIS CURRENT AUDIO FADE OUT
              audioPlayer.stop(audioPlayer.currentSourceId);
              // audioPlayer.fadeOut(audioPlayer.currentSourceId);
              // REPLACE CURRENT AUDIO WITH NEW SOURCE AND FADE IN
              audioPlayer.currentSourceId = nextAudioSourceId;
              // audioPlayer.setVolume(audioPlayer.currentSourceId, 0.0);
              // audioPlayer.play(audioPlayer.currentSourceId);
              audioPlayer.fadeIn(audioPlayer.currentSourceId);
              
              resolve();
            }
        }
      } else {
        resolve();
      }
    })

    let result = await promise;

    return result;
  }

  async checkAutoAudioSource(audioPlayer) {
    let promise = new Promise((resolve, reject) => {

      if (this.data.auto[this.currentAutoId].hasOwnProperty("audioTrack")) {
        let nextAudioSourceState = audioPlayer.getStateBySourceName(audioPlayer.currentSourceFile);

        if (nextAudioSourceState != "UNLOADED" || nextAudioSourceState != "LOADING" || nextAudioSourceState != undefined) {
            // GET NEXT AUDIO TRACK ID
            let nextAudioSourceId = audioPlayer.getIdBySourceName(audioPlayer.currentSourceFile);
            if (nextAudioSourceId != undefined) {
              // THIS CURRENT AUDIO FADE OUT
              audioPlayer.stop(audioPlayer.currentSourceId);
              // audioPlayer.fadeOut(audioPlayer.currentSourceId);
              // REPLACE CURRENT AUDIO WITH NEW SOURCE AND FADE IN
              audioPlayer.currentSourceId = nextAudioSourceId;
              // audioPlayer.setVolume(audioPlayer.currentSourceId, 0.0);
              // audioPlayer.play(audioPlayer.currentSourceId);
              audioPlayer.fadeIn(audioPlayer.currentSourceId);
              
              resolve();
            }
        }
      } else {
        resolve();
      }
    })

    let result = await promise;

    return result;
  }

  async checkRandomAudioSource(audioPlayer) {
    let promise = new Promise((resolve, reject) => {

      if (this.data.random[this.currentRandomId].hasOwnProperty("audioTrack")) {
        let nextAudioSourceState = audioPlayer.getStateBySourceName(audioPlayer.currentSourceFile);

        if (nextAudioSourceState != "UNLOADED" || nextAudioSourceState != "LOADING" || nextAudioSourceState != undefined) {
            // GET NEXT AUDIO TRACK ID
            let nextAudioSourceId = audioPlayer.getIdBySourceName(audioPlayer.currentSourceFile);
            if (nextAudioSourceId != undefined) {
              // THIS CURRENT AUDIO FADE OUT
              audioPlayer.stop(audioPlayer.currentSourceId);
              // audioPlayer.fadeOut(audioPlayer.currentSourceId);
              // REPLACE CURRENT AUDIO WITH NEW SOURCE AND FADE IN
              audioPlayer.currentSourceId = nextAudioSourceId;
              // audioPlayer.setVolume(audioPlayer.currentSourceId, 0.0);
              // audioPlayer.play(audioPlayer.currentSourceId);
              audioPlayer.fadeIn(audioPlayer.currentSourceId);
              
              resolve();
            }
        }
      } else {
        resolve();
      }
    })

    let result = await promise;

    return result;
  }

  setPlayerNewParameters(videoPlayer, audioPlayer) {
    audioPlayer.setRate(audioPlayer.currentSourceId, audioPlayer.playbackRate);
    // audioPlayer.setVolume(audioPlayer.currentSourceId, audioPlayer.volume);

    videoPlayer.setPlaybackRate(videoPlayer.playbackRate);
    videoPlayer.seekTo(this.timecode);
  }

  async setPlayerNewParametersAsync(videoPlayer, audioPlayer) {
    let _this = this;

    let promise = new Promise((resolve, reject) => {        
      videoPlayer.setPlaybackRate(videoPlayer.playbackRate);
      videoPlayer.player.seekTo(_this.timecode, true);
      videoPlayer.getState();

      audioPlayer.setRate(audioPlayer.currentSourceId, audioPlayer.playbackRate);

      if (videoPlayer.state == "PLAYING") {
        resolve(_this.timecode);
      }
    })

    let result = await promise;

    return result;
  }

  async setPlayerNewDialogParametersAsync(videoPlayer, audioPlayer) {
    let _this = this;

    let promise = new Promise((resolve, reject) => {        
      videoPlayer.setPlaybackRate(videoPlayer.playbackRate);
      videoPlayer.player.seekTo(_this.timecode, true);
      videoPlayer.getState();

      audioPlayer.setRate(audioPlayer.currentSourceId, audioPlayer.playbackRate);
      audioPlayer.player[audioPlayer.currentSourceId].seek(_this.timecode);

      if (videoPlayer.state == "PLAYING") {
        resolve(_this.timecode);
      }
    })

    let result = await promise;

    return result;
  }

}