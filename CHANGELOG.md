# CHANGELOG

## LOV. 

---

## [v.0.0.4] - 2018-10-15

### ADDED

- fade in audio after each transition

### CHANGED

- timeline order divided in 3 scenes
- mobile load-cover text font-size

### REMOVED

- reverse shot dialog solo switch

---

## [v.0.0.3] - 2018-10-08

### ADDED

- "story" and "howto" modal at load time
- Hugo and Charles solo dialogs
- solo dialog subtitles
- dialog JSON data object
- link to player from laurentgrey.com/LOV.html
- reverse shot dialog solo switch
- player refresh rate depending on device type
- lov-og-image for thumbnail preview

### FIXED

- reduced load-modal font-size on mobile

### CHANGED

- total time set to 223 seconds
- random timeline bounds
- dial_intro sound file amplification
- dial_intro audio encoded to 64kbps
- load-cover ready state duration
- setPlayerNewParameters function now async
- key/tap events limit is set dynamically depending on scene marker length

### REMOVED

- extreme video and audio playback rate changes
- key/tap event limit
- humoristic text

---

## [v.0.0.2] - 2018-08-18

### ADDED

- desktop loading screen
- modal window border
- tap button on tablet and mobile to trigger timeline events, one in portrait mode, two buttons in landscape mode
- mouse icon state handling at load time

### FIXED

- prevent player URL redirection
- center modal window on mobile
- no white line on player border
- bug where previous audio source is played again if video is paused then key/touch event is triggered

### CHANGED

- border on player left and right side are thinner
- subtitles translation and timing

### REMOVED

- delay between each marker transition
- subtitles underline

---

## [v.0.0.1] - 2018-08-13

### ADDED

- README.md
- CHANGELOG.md
- Youtube IFrame API for video streaming
- Web Audio API with Howler.js for audio playback
- spacebar and touch events interaction acting on following audio/video parameters:
  - timecode
  - video playback
  - audio track
  - audio playback
- time delay between user interaction
- user maximum interaction limit
- desktop/tablet/mobile responsive design
- glow effect on text
- fade-in effect after loading
- modal windows to display content with vue-js-modal
- loading screen on mobile
- warning message on unsupported mobile devices
- inline Youtube player to watch the video inside the web page and not fullscreen
- Youtube IFrame API playVideo() call supported on mobile with first user interaction on a transparent iframe
- auto unlock of audio sources in mobile with Howler.js
- menu video screen
- Javascript Promise used for asynchronous loading of audio sources and linear chain of events
- audio sources re-amplification
- concatenation of all dialog audio sources into one file for simpler video sync
- .mp3 bitrate compression to 64kbps for faster loading on mobile
- .mp3 to .webm convertion
- video playbackRate and audio playbackRate change sync
- .WEBVTT timecode format converted to .SMI for custom subtitles style
- timeline data read from external JSON file
- toggle DEBUGMODE with Konami Code input
- app auto-reload when timeline ends