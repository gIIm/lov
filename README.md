# LOV
### LOV. interactive video player

current version `v.0.0.4`, developped with Vue.js, Youtube IFrame API and Howler.js

 **Supported on:**

```
- Google Chrome, Mozilla Firefox, Safari
- iOS > 9.0
- Android > 8.0
```

**External dependencies:**

```
- vue
- vue-youtube
- vue-js-modal
- node-sass
- sass-loader
- axios
- howler
- mobile-device-detect
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```
