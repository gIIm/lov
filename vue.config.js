module.exports = {
		baseUrl: process.env.NODE_ENV === 'production' ? './lov': './',
	  configureWebpack: {
    	entry: {
    	  app: './src/index.js'
		}
	}
}
